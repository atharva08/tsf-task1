from django.db import models
from django.utils.timezone import now

# Create your models here.

class User(models.Model):
    name=models.CharField(max_length=40)
    email=models.CharField(max_length=40)
    contact=models.CharField(max_length=10)
    desc=models.CharField(max_length=700)
    current_points=models.CharField(max_length=40)
    pic_path=models.FileField(null=True)

class Credit(models.Model):
    who_transfered=models.ForeignKey(User,on_delete=models.CASCADE,related_name='who_transfered')
    to_whom=models.ForeignKey(User,on_delete=models.CASCADE,related_name='to_whom')
    credits_transfered=models.CharField(max_length=40)
    update_time=models.DateField(default=now)

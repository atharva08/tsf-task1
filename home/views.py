from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.urls import reverse
from home.models import User,Credit
from django.db.models import Q
from django.contrib import messages


def home(request):
    u=User.objects.all().order_by('id')
    return render(request,'home/index.html',{'users':u})


def UserDetails(request,u_id):
    u=User.objects.get(id=u_id)
    other_users= User.objects.exclude(id=u_id)
    points_given=Credit.objects.filter(who_transfered=u_id)
    points_received=Credit.objects.filter(to_whom=u_id)
    #print(points_received)
    return render(request,'home/UserDetails.html',{'user':u,'other_users':other_users,'points_received':points_received,'points_given':points_given})

def TransferAction(request):
    fromm=request.POST.get('fromm','NULL')
    to=request.POST.get('to_whom','NULL')
    points=request.POST.get('points','NULL')

    who_transfered=User.objects.get(id=fromm)
    to_whom=User.objects.get(id=to)

    c=Credit(who_transfered=who_transfered,to_whom=to_whom,credits_transfered=points)
    c.save()


    who_transfered.current_points=str(int(who_transfered.current_points)-int(points))
    who_transfered.save()

    to_whom.current_points=str(int(to_whom.current_points)+int(points))
    to_whom.save()
    messages.success(request, 'Successfully transfered '+str(points)+' points to '+str(to_whom.name))

    #return HttpResponse('hii')
    return HttpResponseRedirect(reverse('UserDetails', kwargs={'u_id':fromm}))

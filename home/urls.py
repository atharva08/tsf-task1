from django.urls import path
from . import views

urlpatterns=[
    path('',views.home,name='home'),
    path('UserDetails/<str:u_id>',views.UserDetails,name='UserDetails'),
    path('TransferAction',views.TransferAction,name='TransferAction'),
    ]
